import {Component, OnInit} from '@angular/core';
import {Pelicula} from './pelicula';
import {PeliculaService} from './pelicula.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
})
export class PeliculasComponent implements OnInit {

  peliculas: Pelicula[];
  p: number = 1;
  public searchTitle: string = '';
  public searchId: string = '';
  firebase:boolean = true;

  constructor(private  peliculaService: PeliculaService, private router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.ngOnInit();
      }
    });
  }


  ngOnInit() {
    if(this.router.url.startsWith('/viewList')){
      this.firebase = false;
      this.peliculaService.getRestApi().subscribe((peliculas) => this.peliculas = peliculas);
    } else this.peliculaService.getPeliculas().subscribe((peliculas) => this.peliculas = peliculas);
  }

}
