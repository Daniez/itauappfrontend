import { Component, OnInit } from '@angular/core';
import {Pelicula} from './pelicula';
import {PeliculaService} from './pelicula.service';
import {ActivatedRoute, Router} from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent implements OnInit {

  private pelicula: Pelicula = new Pelicula();
  titulo = 'Crear Pelicula';
  esCrear:boolean = true;

  constructor(private peliculaService: PeliculaService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    if(this.router.url.startsWith('/editItem')){
       this.esCrear = false;
       this.titulo = 'Editar Pelicula';
    }
    this.cargarPelicula();
  }

  cargarPelicula(): void {

    this.activatedRoute.params.subscribe(params => {
      let id = params.id;
      if (id) {
        this.peliculaService.getPelicula(id).subscribe((pelicula) => {this.pelicula = pelicula
          if (pelicula.idFirebase == null) {
            this.router.navigate(['/viewTodos']);
            swal('No se encontró pelicula', ``, 'error');
          }
        });
      }
    });
  }

  public crear(): void {
      if (!this.validarCampos()) {return}
      this.peliculaService.crear(this.pelicula).subscribe(
        response => {
          if (response.idFirebase == null) {
            this.router.navigate(['/viewTodos']);
            swal('Nueva Pelicula Agregada', `Pelicula ${response.title}`, 'success');
          } else {
            swal('El id de la pelicula ya existe, editelá o agregue otra distinta', `Id Pelicula:  ${response.id}`, 'error');
          }
          });
  }

  public actualizar(): void {
    if (!this.validarCampos()) {return}
    this.peliculaService.editar(this.pelicula).subscribe(
      response => {
          this.router.navigate(['/viewTodos']);
          swal('Se actualizó la ', `pelicula ${response.title}`, 'success');
      });
  }

  public eliminar(): void {
    this.peliculaService.eliminar(this.pelicula).subscribe(
      response => {
        this.router.navigate(['/viewTodos']);
        swal('Se eliminó la ', `pelicula ${response.title}`, 'success');
      });
  }

  private validarCampos():boolean{
    if (this.pelicula.title == null || this.pelicula.title == null || this.pelicula.title === '' || this.pelicula.title === '') {
      swal('El id y el titulo son obligatorios', ``, 'error');
      return false
    } else return true;
  }
}
