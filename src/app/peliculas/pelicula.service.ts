import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pelicula} from './pelicula';
import {ActivatedRoute} from  '@angular/router';
// import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PeliculaService {
  private urlEndPoint = 'http://localhost:8080/firebase/peliculas';
  private urlRestApi = 'https://jsonplaceholder.typicode.com/todos';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json;charset=utf-8'});

  constructor( private http: HttpClient) { }



  getPeliculas(): Observable<Pelicula[]> {
    return this.http.get<Pelicula[]>(this.urlEndPoint);
    // return this.http.get<Pelicula[]>(this.urlEndPoint).pipe(map(response => response as Pelicula[]));
  }

  getRestApi(): Observable<Pelicula[]> {
    return this.http.get<Pelicula[]>(this.urlRestApi);
    // return this.http.get<Pelicula[]>(this.urlEndPoint).pipe(map(response => response as Pelicula[]));
  }
  crear(pelicula: Pelicula): Observable<Pelicula> {
      return this.http.post<Pelicula>(`${this.urlEndPoint}/crear`, pelicula, {headers: this.httpHeaders});
  }

  editar(pelicula: Pelicula): Observable<Pelicula> {
    return this.http.put<Pelicula>(`${this.urlEndPoint}/editar`, pelicula, {headers: this.httpHeaders});
  }

  getPelicula(id): Observable<Pelicula> {
    return this.http.get<Pelicula>(`${this.urlEndPoint}/pelicula/${id}` );
  }

  eliminar(pelicula: Pelicula): Observable<Pelicula> {
    return this.http.delete<Pelicula>(`${this.urlEndPoint}/eliminar/${pelicula.id}`, {headers: this.httpHeaders});
  }

}
