import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
  name: 'filter'
})
@Injectable()
export class FilterPipe implements PipeTransform {
  transform(value: any, args: any[]): any {
    if ((args[0] === '' && args[1] === '')) return value;
    const resultPeliculas = [];
    for (const pelicula of value) {
      if ((args[0].length > 0) && String(pelicula.id).toLowerCase().indexOf(args[0].toLowerCase()) > -1) {
        if(!resultPeliculas.includes(pelicula))
          resultPeliculas.push(pelicula);
      };
      if ((args[1].length > 0 ) && pelicula.title.toLowerCase().indexOf(args[1].toLowerCase()) > -1) {
        if(!resultPeliculas.includes(pelicula))
          resultPeliculas.push(pelicula);
      };
    };
    return resultPeliculas;
  }
}
