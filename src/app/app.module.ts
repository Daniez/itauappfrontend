import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {FormsModule} from '@angular/forms';
import { PeliculasComponent } from './peliculas/peliculas.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { FormComponent } from './peliculas/form.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {FilterPipe} from './peliculas/filter.pipe';



const routes: Routes = [
  {path: '', redirectTo: '/viewList' , pathMatch: 'full'},
  {path: 'viewList', component: PeliculasComponent },
  {path: 'createItem', component: FormComponent },
  {path: 'editItem/:id', component: FormComponent },
  {path: 'viewTodos', component: PeliculasComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PeliculasComponent,
    FormComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    RouterModule.forRoot(routes)

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
